<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

class MotorFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'mesin' => $this->faker->words(2, true),
            'tipe_suspensi' => $this->faker->word(),
            'tipe_transmisi' => $this->faker->randomElement(['Matic', 'Manual'])
        ];
    }
}
