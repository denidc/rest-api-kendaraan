<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

class KendaraanFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'tahun_kendaraan' => $this->faker->year(),
            'warna' => $this->faker->safeColorName(),
            'harga' => $this->faker->randomNumber(7, true)
        ];
    }
}
