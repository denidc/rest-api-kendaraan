<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

class MobilFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'mesin' => $this->faker->words(2, true),
            'kapasitas_penumpang' => random_int(2, 4),
            'tipe' => $this->faker->word()
        ];
    }
}
