<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Kendaraan;
use App\Models\Motor;

class MotorSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $kendaraans = Kendaraan::all();

        foreach($kendaraans as $kendaraan) {
            $motor = Motor::factory()->count(10)->create([
                'kendaraans_id' => $kendaraan->id
            ]);
        }
    }
}
