<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Kendaraan;
use App\Models\Mobil;

class MobilSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $kendaraans = Kendaraan::all();

        foreach($kendaraans as $kendaraan) {
            $mobils = Mobil::factory()->count(10)->create([
                'kendaraans_id' => $kendaraan->id
            ]);
        }
    }
}
