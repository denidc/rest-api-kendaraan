<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Auth\JWTAuthController;
use App\Http\Controllers\KendaraanController;
use App\Http\Controllers\MotorController;
use App\Http\Controllers\MobilController;
use App\Http\Controllers\TransaksiController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

//Auth
Route::post('register', [JWTAuthController::class, 'register']);
Route::post('login', [JWTAuthController::class, 'login']);

Route::group(['middleware' => 'jwt.auth'], function () {
    //Auth
    Route::post('logout', [JWTAuthController::class, 'logout']);

    //Kendaraan
    Route::get('/all', [KendaraanController::class, 'allAvailable']);
    Route::get('/motor', [MotorController::class, 'motorAvailable']);
    Route::get('/mobil', [MobilController::class, 'mobilAvailable']);

    //Transaksi
    Route::post('/payment', [TransaksiController::class, 'addPayment']);
    Route::get('/payment-list', [TransaksiController::class, 'paymentList']);
    Route::put('/checkout/{id}', [TransaksiController::class, 'checkout']);

    //History Transaksi
    Route::get('/history', [TransaksiController::class, 'history']);
    Route::get('/history-detail/{id}', [TransaksiController::class, 'historyDetail']);
});
