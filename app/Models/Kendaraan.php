<?php

namespace App\Models;

use Tymon\JWTAuth\Contracts\JWTSubject;
use Jenssegers\Mongodb\Eloquent\Model as Eloquent;
use Illuminate\Database\Eloquent\Factories\HasFactory;

use App\Models\Mobil;
use App\Models\Motor;
use App\Models\Transaksi;

class Kendaraan extends Eloquent implements JWTSubject
{
    use HasFactory;

    // protected $connection = 'mongodb';
    protected $collection = 'kendaraans';

    protected $fillable = [
        'tahun_keluaran', 'warna', 'harga'
    ];

    public function mobil(){
        return $this->belongsToMany(Mobil::class);
    }

    public function motor(){
        return $this->belongsToMany(Motor::class);
    }

    public function transaksi(){
        return $this->belongsToMany(Transaksi::class);
    }

    /**
     * Get the identifier that will be stored in the subject claim of the JWT.
     *
     * @return mixed
     */
    public function getJWTIdentifier()
    {
        return $this->getKey();
    }

    /**
     * Return a key value array, containing any custom claims to be added to the JWT.
     *
     * @return array
     */
    public function getJWTCustomClaims()
    {
        return [];
    }
}
