<?php

namespace App\Models;

use Tymon\JWTAuth\Contracts\JWTSubject;
use Jenssegers\Mongodb\Eloquent\Model as Eloquent;
use Illuminate\Database\Eloquent\Factories\HasFactory;

use App\Models\Kendaraan;

class Motor extends Eloquent implements JWTSubject
{
    use HasFactory;

    protected $collection = 'motors';

    protected $fillable = [
        'kendaraans_id', 'mesin', 'tipe_suspensi','tipe_transmisi'
    ];

    public function kendaraan(){
        return $this->belongsTo(Kendaraan::class);
    }

    /**
     * Get the identifier that will be stored in the subject claim of the JWT.
     *
     * @return mixed
     */
    public function getJWTIdentifier()
    {
        return $this->getKey();
    }

    /**
     * Return a key value array, containing any custom claims to be added to the JWT.
     *
     * @return array
     */
    public function getJWTCustomClaims()
    {
        return [];
    }
}
