<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

use App\Repository\KendaraanRepository;
use App\Models\Kendaraan;

use App\Repository\MobilRepository;
use App\Models\Mobil;

use App\Repository\MotorRepository;
use App\Models\Motor;

use App\Repository\TransaksiRepository;
use App\Models\Transaksi;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(KendaraanRepository::class, function ($app) {
            return new KendaraanRepository($app->make(Kendaraan::class));
        });

        $this->app->bind(MobilRepository::class, function ($app) {
            return new MobilRepository($app->make(Mobil::class));
        });

        $this->app->bind(MotorRepository::class, function ($app) {
            return new MotorRepository($app->make(Motor::class));
        });

        $this->app->bind(TransaksiRepository::class, function ($app) {
            return new TransaksiRepository($app->make(Transaksi::class));
        });
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
