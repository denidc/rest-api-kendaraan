<?php
namespace App\Repository;

class KendaraanRepository {

  protected $model = 'kendaraans';

  public function __construct($model)
  {
    $this->model = $model;
  }

  public function getKendaraan($id)
  {
    return $this->model->where('_id', $id)->first();
  }

  public function getAllAvailable()
  {
    return $this->model->with(['motor', 'mobil'])->whereDoesntHave('transaksi')->get();
  }

}
