<?php
namespace App\Repository;

class MobilRepository {

  protected $model = 'mobils';

  public function __construct($model)
  {
    $this->model = $model;
  }

  public function getMobil($id)
  {
    return $this->model->where('_id', $id)->first();
  }

  public function getMobilAvailable()
  {
    return $this->model->get();
  }
}
