<?php
namespace App\Repository;

class MotorRepository {

  protected $model = 'motors';

  public function __construct($model)
  {
    $this->model = $model;
  }

  public function getMotor($id)
  {
    return $this->model->where('_id', $id)->first();
  }

  public function getMotorAvailable()
  {
    return $this->model->get();
  }
}
