<?php
namespace App\Repository;

class TransaksiRepository {

  protected $model = 'transaksis';

  public function __construct($model)
  {
    $this->model = $model;
  }

  public function listPay(){
    return $this->model->where('status', 'pending')->get();
  }

  public function addPay($data){
    return $this->model->create($data)->save();
  }

  public function checkoutItem($id){
    return $this->model->where('_id', $id)->update(['status' => 'done']);
  }

  public function getPaymentItem($id)
  {
    return $this->model->where('_id', $id)->where('status', 'pending')->first();
  }

  public function getPendingItem()
  {
    return $this->model->where('status', 'pending')->first();
  }

  public function getDoneItem()
  {
    return $this->model->where('status', 'done')->get();
  }

  public function getDoneItemDetail($id)
  {
    return $this->model->where('_id', $id)->first();
  }
}
