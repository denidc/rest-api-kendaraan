<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Repository\KendaraanRepository;

class KendaraanController extends Controller
{
    public function allAvailable(KendaraanRepository $repository)
    {
      $all = $repository->getAllAvailable();

      return response()->json(['all' => $all, 'status_code' => 200]);
    }
}
