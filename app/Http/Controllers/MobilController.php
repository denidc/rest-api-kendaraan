<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Repository\MobilRepository;

class MobilController extends Controller
{
    public function mobilAvailable(MobilRepository $repository)
    {
      $mobils = $repository->getMobilAvailable();

      return response()->json(['mobils' => $mobils, 'status_code' => 200]);
    }
}
