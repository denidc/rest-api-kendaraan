<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Repository\MotorRepository;

class MotorController extends Controller
{
    public function motorAvailable(MotorRepository $repository)
    {
      $motors = $repository->getMotorAvailable();

      return response()->json(['motors' => $motors, 'status_code' => 200]);
    }
}
