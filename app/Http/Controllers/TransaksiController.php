<?php

namespace App\Http\Controllers;

use Validator;
use Illuminate\Http\Request;

use App\Repository\TransaksiRepository;
use App\Repository\MobilRepository;
use App\Repository\MotorRepository;
use App\Repository\KendaraanRepository;

class TransaksiController extends Controller
{
    public function addPayment(TransaksiRepository $transaksiRepo, MobilRepository $mobilRepo,
            MotorRepository $motorRepo, KendaraanRepository $kendaraanRepo, Request $request){

        $validator = Validator::make($request->all(),[
            'tipe_kendaraan' => 'required',
            'id_kendaraan' => 'required'
        ]);

        if ($validator->fails()) {
            return response()->json(['error'=>$validator->errors()], 401);
        }

        $trans = $transaksiRepo->getPendingItem();

        if(empty($trans)){
            if($request->tipe_kendaraan == 'motor'){
                $kendaraan = $motorRepo->getMotor($request->id_kendaraan);
            }else if($request->tipe_kendaraan == 'mobil'){
                $kendaraan = $mobilRepo->getMobil($request->id_kendaraan);
            }else{
                return response()->json(['message' => 'Data yang Anda masukkan tidak sesuai'], 401);
            }

            $tran = [
                'motors_id' => ($request->tipe_kendaraan == 'motor') ? $kendaraan->id : "",
                'mobils_id' => ($request->tipe_kendaraan == 'mobil') ? $kendaraan->id : "",
                'tipe_kendaraan' => $request->tipe_kendaraan,
                'harga' => $kendaraanRepo->getKendaraan($kendaraan->kendaraans_id)->harga,
                'status' => 'pending',
            ];

            $transaksi = $transaksiRepo->addPay($tran);

            return response()->json(['message' => 'Sukses Menambahkan Kendaraan ke Pembayaran'], 200);
        }else{
            return response()->json(['message' => 'Anda masih mempunyai Kendaraan yang belum dibayar'], 401);
        }
    }

    public function paymentList(TransaksiRepository $repository){
        $pays = $repository->listPay();

        return response()->json(['pays' => $pays, 'status_code' => 200]);
    }

    public function checkout(TransaksiRepository $repository, $id){
        $trans = $repository->getPaymentItem($id);

        if(!empty($trans)){
            $transaksi = $repository->checkoutItem($id);

            return response()->json(['message' => 'Sukses checkout Kendaraan Anda'], 200);
        }else{
            return response()->json(['message' => 'Anda tidak mempunyai Kendaraan untuk di checkout'], 401);
        }
    }

    public function history(TransaksiRepository $repository){
        $his = $repository->getDoneItem();

        return response()->json(['history' => $his, 'status_code' => 200]);
    }

    public function historyDetail(TransaksiRepository $repository, $id){
        $his = $repository->getDoneItemDetail($id);

        return response()->json(['history' => $his, 'status_code' => 200]);
    }
}
