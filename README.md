## Instalasi Rest Api Kendaraan Laravel

- Instal komponen-komponen composer di project laravel terlebih dahulu dengan cara `composer install`
- Ubah .env.example menjadi .env dan konfigurasi koneksi database mongodb nya
- Lalu jalankan perintah `php artisan migrate:fresh --seed` untuk membuat data dummy di mongodb
- Selanjutnya jalankan perintah `php artisan serve` untuk menjalankan project Rest Api Kendaraan.
