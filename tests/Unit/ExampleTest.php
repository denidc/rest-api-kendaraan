<?php

namespace Tests\Unit;

use PHPUnit\Framework\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Auth;
use App\Models\Transaksi;

class ExampleTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
    public function test_example()
    {
        $tran = new Transaksi([
            'motors_id' => '',
            'mobils_id' => '12345',
            'tipe_kendaraan' => 'motor',
            'harga' => 500000,
            'status' => 'pending',
        ]);

        $this->assertEquals('', $tran->motors_id);
    }
}
